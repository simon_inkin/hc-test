# README #

A little read me for HashCoins

### What is this repository for? ###

For nice people of HashCoins OÜ =)

### How do I get set up? ###

The project is made using Silex micro framework

MVC Silex template was taken from here:
https://github.com/aptoma/silex-bootstrap

Therefore, the installation is the same as specified in the link above.
Basically, this is a configured clone of aptoma/silex-bootstrap with a couple of controllers and the Slack+CryptoPay handler.
So, just clone the project from this repo and use the **installation instructions** (not the repo) from aptoma/silex-bootstrap page.

Also, although my own DB config is in the repo, the script would not connect to the DB from the outside source without manually adding the source IP in the Zone.ee config panel.
Please use the provided SQL (database.sql in root) file to set up you own DB if you want to test the app =)
DB config is in the app.php file

Check the video at
http://www.limewave.org/hashcoins-demosntration.mp4
for the initial slash command setup and a little demonstration

### Available Slack commands ###

First, the "slash command" needs to be specified and both the command and the API key added to the 'slack_commands' table in the DB

After that the user can use the following commands (using '/hc' as example slash command):

**(1) Register CryptoPay API key**

command: '/hc register %cryptopay API key%'

expected: "Your CryptoPay API key has been changed!"


The user must first register himself with the system, providing the CP API key via Slack. The key is then assigned per Slack user_id.

**(2) CP Rates command:**

command: '/hc rates'


expected: "Current CryptoPay rates are as follows: GBP: 314.412 | EUR: 396.1867 | BTC: 396.1867 | USD: 450.7951"

**(3) CP Balance command:**

command: '/hc balance'


Unfortunately this command doesn't work due to problems on the CryptoPay side. Although the v1 API reference has very specific guidelines for this command, it still returns 404 when issued. Nothing can be done about it, except using the v2 API, which is totally different from v1

**(4) CP Invoice command:**

command: '/hc invoice %CryptoPay invoice UUID%'

expected: "
Here is information about your invoice:

UUID: 1fadf068-5e12-4c37-b972-80d3847ae18d
Description:  Mike's coffee = BTC
Date created: 14:05:28 5 May 2016
Valid until: 14:05:28 5 May 2016
Bitcoin: 3NHu7BTjqLhSx4qBUs9hujAprUiRj5ri19
Price: 0.0953 BTC
Received: 0.0 BTC
"


or the "all invoices command"


command: '/hc invoice'
expected: "
Here is information about all your invoices:

/* followed by an array of all invoices */
"


**(5) CP Pay command**
(the most important)

command: '/hc pay 300 eur Mike's coffee'

expected: "
Waiting for payment: ​*0.0953 BTC*​ to 3NHu7BTjqLhSx4qBUs9hujAprUiRj5ri19
bitcoin:3NHu7BTjqLhSx4qBUs9hujAprUiRj5ri19?amount=0.0953
%QR image of the address%
"


CP Pay parameter order doesn't matter, i.e. "/hc pay Mike's eur 300 coffee" would have identical result as the command above.


P.S. What's up with this readme formatting? It keeps glitching...