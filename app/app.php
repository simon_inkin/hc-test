<?php

$startTime = microtime(true);
require_once __DIR__ . '/../vendor/autoload.php';

$config = array(
    'debug' => true,
    'timer.start' => $startTime,
    'monolog.name' => 'silex-bootstrap',
    'monolog.level' => \Monolog\Logger::DEBUG,
    'monolog.logfile' => __DIR__ . '/log/app.log',
    'twig.path' => __DIR__ . '/../src/App/views',
    'twig.options' => array(
        'cache' => __DIR__ . '/cache/twig',
    ),
);

if (file_exists(__DIR__ . '/config.php')) {
    include __DIR__ . '/config.php';
}

$app = new App\Silex\Application($config);

# Components
$app->register(new Silex\Provider\SessionServiceProvider());
$app->register(new Silex\Provider\DoctrineServiceProvider(), [
    'db.options' => [
        'driver' => 'pdo_mysql', 
        'dbname' => 'd10292sd118398',
        'host' => 'd10292.mysql.zone.ee',
        'user' => 'd10292sa112124',
        'password' => '76ubTW',
        'charset' => 'utf8'
    ]
]);

# Controllers
$app['app.default_controller'] = $app->share(
    function () use ($app) {
        return new \App\Controller\DefaultController($app['twig'], $app['logger']);
    }
);
$app['app.hashcoins_controller'] = $app->share(
    function () use ($app) {
        return new \App\Controller\HashcoinsController($app);
    }
);

include __DIR__ . '/routing.php';

return $app;
