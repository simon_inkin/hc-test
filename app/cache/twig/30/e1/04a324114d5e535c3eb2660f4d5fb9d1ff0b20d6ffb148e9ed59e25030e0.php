<?php

/* hashcoins.twig */
class __TwigTemplate_30e104a324114d5e535c3eb2660f4d5fb9d1ff0b20d6ffb148e9ed59e25030e0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("layout.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "Check back later..";
    }

    // line 4
    public function block_content($context, array $blocks = array())
    {
        // line 5
        echo "<div style=\"width: 100%; height: 100%; display: table; text-align: center;\">
    <p class=\"heading\" style=\"display: table-cell; vertical-align: middle;\">HashCoins Slack Button here</p>
</div>
";
    }

    public function getTemplateName()
    {
        return "hashcoins.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 5,  35 => 4,  29 => 2,);
    }
}
