<?php

/* index.twig */
class __TwigTemplate_af6c82848083c90c00cc7ff1628b430e186d6b0976b0f5c8bdc254327206ef44 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("layout.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "Check back later..";
    }

    // line 4
    public function block_content($context, array $blocks = array())
    {
        // line 5
        echo "<div style=\"width: 100%; height: 100%; display: table; text-align: center;\">
    <p class=\"heading\" style=\"display: table-cell; vertical-align: middle;\">Check back later..</p>
</div>
";
    }

    public function getTemplateName()
    {
        return "index.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 5,  35 => 4,  29 => 2,);
    }
}
