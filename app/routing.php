<?php

$app->get('/', 'app.default_controller:index');
$app->get('/hashcoins', 'app.hashcoins_controller:index');
$app->post('/hashcoins/slack', 'app.hashcoins_controller:slack');