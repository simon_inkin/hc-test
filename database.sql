-- phpMyAdmin SQL Dump
-- version 4.6.0
-- http://www.phpmyadmin.net
--
-- Host: d10292.mysql.zone.ee
-- Generation Time: May 08, 2016 at 11:37 PM
-- Server version: 5.6.28-log
-- PHP Version: 5.5.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `d10292sd118398`
--

-- --------------------------------------------------------

--
-- Table structure for table `cryptopay_keys`
--

CREATE TABLE `cryptopay_keys` (
  `user_id` varchar(50) NOT NULL,
  `team_id` varchar(50) NOT NULL,
  `api_key` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `slack_commands`
--

CREATE TABLE `slack_commands` (
  `id` int(11) NOT NULL,
  `command` enum('/hc-test','hc') NOT NULL,
  `token` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cryptopay_keys`
--
ALTER TABLE `cryptopay_keys`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `slack_commands`
--
ALTER TABLE `slack_commands`
  ADD PRIMARY KEY (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
