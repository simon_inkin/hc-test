<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use App\Utility\Slack;

use Psr\Log\LoggerInterface;

class HashcoinsController {

	/**
     * @var \Twig_Environment
     */
    private $twig;

	/**
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * @var DoctrineServiceProvider
	 */
	private $db;

	/**
	 * @var SessionServiceProvider
	 */
	private $session;


	private $app;

	public function __construct($app) {
		$this->twig = $app['twig'];
		$this->logger = $app['logger'];

		$this->db = $app['db'];
		$this->session = $app['session'];
		$this->app = $app;
	}

	public function index() {
		$this->logger->debug('Executing HashcoinsController::index');
        return $this->twig->render('hashcoins.twig');
	}

	public function slack(Request $request) {
		$this->logger->debug('Executing HashcoinsController::slack');

		$response = new Response();
		$response->setStatusCode(Response::HTTP_OK);

		$slack = new Slack($this->db, $this->session);
		$reply = $slack->execute($request);

		if (is_object($reply)) {
			$response->headers->set('Content-Type', 'application/json');
			$response->setContent((json_encode($reply)) ?: 'Error: JSON encoding failed');
		} else {
			$response->headers->set('Content-Type', 'text/plain');
			$response->setContent((is_string($reply)) ? $reply : 'Error: Incorrect reply');
		}

		return $response;
	}
}