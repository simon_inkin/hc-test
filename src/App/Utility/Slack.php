<?php

	namespace App\Utility;

	use Doctrine\DBAL\Connection;
	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Component\HttpFoundation\Session\Session;

	class Slack {

		/**
		 * @var Required values for a valid request
		 */
		private $_required = [ 'token', 'team_id', 'channel_id', 'user_id', 'command', 'text', 'response_url' ];

		/**
		 * @var Allowed command methods
		 */
		private $_allowedMethods = [ 'register', 'pay', 'invoice', 'balance', 'rates' ];
		// v1 API balance command gives 404 =(

		/**
		 * @var Allowed payment currencies
		 */
		private $_allowedCurrencies = [ 'EUR', 'GBP', 'USD', /*'BTC'*/ ];

		/**
		 * @var Default payment currency
		 */
		private $_defaultCurrency = 'EUR';

		/**
		 * @var CryptoPay API URL
		 */
		private $_url = 'https://cryptopay.me/api/v1';

		/**
		 * @var Logged in user
		 */
		private $user;

		/**
		 * @var DoctrineServiceProvider
		 */
		private $db;

		/**
		 * @var SessionServiceProvider
		 */
		private $session;

		function __construct(Connection $db, Session $session) {
			$this->db = $db;
			$this->session = $session;
		}

		public function execute(Request $request) {
			if (!$this->_isValidRequest($request)) {
				return 'Error: Invalid request';
			}
			if (!($token = $this->getToken($cmd = ltrim($request->request->get('command'), '/')))) {
				return 'Error: Invalid command';
			}
			if ($request->request->get('token') != $token) {
				return 'Error: Invalid command token';
			}

			$reply = null;
			$args = $request->request->get('text');
			$method = substr($args, 0, strpos($args, ' ') ?: strlen($args));

			if (!in_array($method, $this->_allowedMethods)) {
				return 'Error: Illegal command method';
			}
			if ($this->user = $this->getUser($request->request->get('user_id'))) {
				$reply = call_user_func([$this, 'cp'.ucfirst($method)], ltrim(substr($args, strlen($method)), ' '));
			} else if ($method == 'register') {
				$reply = $this->setUser($request->request->get('user_id'), $request->request->get('team_id'), ltrim(substr($args, strlen($method)), ' '));
			} else {
				$reply = 'Error: User not found. Please register your user\'s CryptoPay API key by running "/hc register %CryptoPay API key%"';
			}

			return $reply;
		}

		public function cpPay($args = null) {
			# ##
			# ok, yeah, it's a sort of a buggy kludge and needs improvements, 
			# but hey, user input is unpredictable =)
			$params = ['price' => null, 'currency' => null, 'description' => ''];
			$args = explode(' ', $args);
			$len = count($args);
			for ($i = $len-1; $i >= 0; $i--) {
				if (is_numeric($args[$i])) {
					if (!is_null($params['price'])) {
						return 'Error: Multiple numeric values specified';
					}
					$params['price'] = $args[$i];
				} else if (is_string($args[$i]) && 
					in_array(strtoupper($args[$i]), $this->_allowedCurrencies)) {
					if (!is_null($params['currency'])) {
						return 'Error: Multiple currencies specified';
					}
					$params['currency'] = strtoupper($args[$i]);
				} else {
					$params['description'] = (($i == 0) ? '' : ' ') . $args[$i] . $params['description'];
				}
			}

			if (is_null($params['price'])) {
				return 'Error: Price not specified';
			}
			if (is_null($params['currency'])) {
				$params['currency'] = $this->_defaultCurrency;
			}
			$params['description'] = preg_replace("/[^A-Za-z0-9#\-_%?!,.\+\/=@\' ]/", '', $params['description']);
			# ##

			$object = new \stdClass();
			$result = $this->_payCrypto('invoices', $params, true);
			if (!$result) {
				$object->text =  "CryptoPay is down";
			} else if (property_exists($result, 'error')) { 
				$object->text = $this->_flattenError($result);
			} else {
				$object->text = 'Your invoice UUID: *' . $result->uuid . "*\n";
				$object->text .= "Waiting for payment: *" . $result->btc_price . ' BTC* to ' . $result->btc_address . "\n";
				$object->attachments = [(object) [
					'text' => $result->bitcoin_uri . "\n",
					'image_url' => 'https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=' . $result->bitcoin_uri
				]];
			}

			return $object;
		}

		/*public function sendPaymentConfirmation($result) {
			$json = new \stdClass();
			$json->text = 
			$json->
			$url = 'https://hooks.slack.com/services/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';
			$result = $this->getResult($url, json_encode($json), true, true);
		}*/

		public function cpInvoice($args = null) {
			$showResult = function($result) {
				$reply = "\n";
				$reply .= "UUID: " . $result->uuid . "\n";
				$reply .= "Description: " . $result->description . " BTC\n";
				$reply .= "Date created: " . date('H:m:s n M Y', $result->created_at) . "\n";
				$reply .= "Valid until: " . date('H:m:s n M Y', $result->valid_till) . "\n";
				$reply .= "Bitcoin: " . $result->btc_address . "\n";
				$reply .= "Price: " . $result->btc_price . " BTC\n";
				$reply .= "Received: " . $result->received . " BTC\n";
				return $reply;
			};

			$url = 'invoices';
			if (is_string($args)) {
				$args = ['uuid' => preg_replace("/[^A-Za-z0-9\-]/", '', $args)];
				$url .= '/' . $args['uuid'];
			}
			$result = $this->_payCrypto($url, $args);
			if (!$result) {
				$reply = "CryptoPay is down";
			} else if (property_exists($result, 'error')) { 
				$reply = $this->_flattenError($result);
			} else {
				if (property_exists($result, 'invoices') && !empty($result->invoices)) {
					$reply = "Here is information about all your invoices: \n";
					# year, I know, without the paginator this will become way too big
					foreach ($result->invoices as $key => $invoice) {
						$reply .= "\nInvoice #" . $key . "\n";
						$reply .= $showResult($invoice);
					}
				} else {
					$reply = "Here is information about your invoice: \n";
					$reply .= $showResult($result);
				}
			}
			
			return $reply;
		}

		public function cpBalance($args = null) {
			$result = $this->_payCrypto('balance'); # 404 Not found =(
			# possibly because API v2 is currently active, so they must've switched off 'balance' for v1 API =(
			if (!$result) {
				$reply = "CryptoPay is down";
			} else if (property_exists($result, 'error')) {
				$reply = $this->_flattenError($result);
			} else {
				$reply = 'Your balances are as follows: ';
				$reply .= $this->_flattenResult($result);
			}
			return $reply;
		}

		public function cpRates($args = null) {
			$result = $this->_payCrypto('rates');
			if (!$result) {
				$reply = "CryptoPay is down";
			} else if (property_exists($result, 'error')) {
				$reply = $this->_flattenError($result);
			} else {
				$reply = 'Current CryptoPay rates are as follows: ';
				$reply .= $this->_flattenResult($result);
			}
			return $reply;
		}

		public function getUser($id) {
			if (is_null($user = $this->session->get('slack.users.'.$id))) {
				$statement = $this->db->prepare('SELECT * FROM cryptopay_keys WHERE user_id = :userId LIMIT 1');
				$statement->execute([':userId' => $id]);
				if ($row = $statement->fetch()) {
					$user = ['id' => $row['user_id'], 'teamId' => $row['team_id'], 'apiKey' => $row['api_key']];
					$this->session->set('slack.users.'.$id, $user);
				}
			}
			return $user;
		}

		public function setUser($userId, $teamId, $apiKey) {
			$apiKey = preg_replace("/[^A-Za-z0-9]/", '', $apiKey);
			$statement = $this->db->prepare('REPLACE INTO cryptopay_keys (user_id, team_id, api_key) VALUES (:userId, :teamId, :apiKey)');
			return ($statement->execute([':userId' => $userId, ':teamId' => $teamId, ':apiKey' => $apiKey])) ? 'Your CryptoPay API key has been changed!' : "Oops.. something went wrong =( \n CryptoPay API key not set";
		}

		public function getToken($cmd) {
			if (is_null($token = $this->session->get('slack.commands.'.$cmd))) {
				$statement = $this->db->prepare('SELECT token FROM slack_commands WHERE command = :command LIMIT 1');
				$statement->execute([':command' => $cmd]);
				if ($row = $statement->fetch()) {
					$token = $row['token'];
					$this->session->set('slack.commands.'.$cmd, $token);
				}
			}
			return $token;
		}

		public function getResult($url, $args = [], $post = false, $json = false) {
			$ch = curl_init($url . ((!$post) ? '?' . http_build_query($args) : ''));

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_VERBOSE, 1);
			curl_setopt($ch, CURLOPT_HEADER, 1);
			if ($post) {
				curl_setopt($ch, CURLOPT_POST, 1);
				if ($json) {
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
				} else {
					curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($args));
				}
			}

			$response = curl_exec($ch);

			$headerSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			$header = substr($response, 0, $headerSize);
			$body = substr($response, $headerSize);

			curl_close($ch);

			return $body;
		}

		private function _payCrypto($cmd, $args = [], $post = false) {
			if ($this->user) {
				$args['api_key'] = $this->user['apiKey'];
				$result = $this->getResult($this->_url . '/' . $cmd, $args, $post);
				return ($result) ? json_decode($result) : false;
			}
			return false;
		}

		private function _isValidRequest(Request $request) {
			foreach ($this->_required as $value) {
				if (!$request->request->has($value)) {
					return false;
				}
			}
			return true;
		}

		private function _flattenResult($result) {
			$reply = '';
			$i = 0;
			$len = count((array) $result);
			foreach ($result as $key => $value) {
				$reply .= $key . ': ' . $value;
				if ($i++ < $len - 1) $reply .= ' | ';
			}
			return $reply;
		}

		private function _flattenError($result) {
			return 'CryptoPay error: ' . $result->error . 
					((property_exists($result, 'message')) ? ' - ' . $result->message : '') . 
					((property_exists($result, 'status')) ? ' (status: ' . $result->status . ')' : '');
		}

	}

?>